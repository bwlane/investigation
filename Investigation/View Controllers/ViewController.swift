//
//  ViewController.swift
//  Investigation
//
//  Created by Brian Lane on 10/26/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import GRDB

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var cards: [ArkhamCard]? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    let networkController = NetworkController.shared
    let reuseIdentifier = "cardCell"
    var selectedCard: ArkhamCard? {
        didSet {
            DispatchQueue.main.async {
                guard let imgStr = self.selectedCard?.imagesrc else { return }
                let str = "https://arkhamdb.com" + imgStr
                self.cardImageView.loadImage(with: str)
            }
        }
    }
    
    @IBOutlet weak var cardImageView: AsyncLoadableImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        reload()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? =
            tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? UITableViewCell // When we have custom cells this will be necessary
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle,
                                   reuseIdentifier: reuseIdentifier)
        }
        guard let card = cards?[indexPath.row] else { return cell! }
        cell!.detailTextLabel?.text = card.typeName
        cell!.textLabel?.text = card.realName
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let card = cards?[indexPath.row] else { return }
        selectedCard = card
    }
    
    //TODO: This is hairy. Let's try to avoid this call back mess. 
    func reload()  {
        // Check if we have a database
        // If not, call to the network
        // Process result of network into database
        do {
            try dbQueue.read { db in
                let cards = try ArkhamCard.fetchAll(db)
                if cards.count > 0 {
                    self.cards = cards
                    print("Loading from the database")
                } else {
                    // we haven't loaded from db, so let's call the network
                    guard let url = URL(string: "https://arkhamdb.com/api/public/cards/") else { return }
                    networkController.getCards(from: url) { [unowned self] cards in
                        print("Loading cards from the network")
                        self.cards = cards
                        if let unwrappedCards = cards {
                            self.selectedCard = unwrappedCards[0]
                            // we have cards. Let's write them to the db
                            do {
                                try dbQueue.write { db in
                                    for card in unwrappedCards {
                                        try card.insert(db)
                                    }
                                }
                            } catch let error as NSError {
                                print("There was an error writing to the database: \(error.description)")
                            }
                        }
                    }
                }
            }
        } catch let error as NSError {
            print("There was an error loading the database: \(error.description)")
        }


    }
    
}

