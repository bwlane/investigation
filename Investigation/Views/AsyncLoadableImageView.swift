//
//  AsyncLoadableImageView.swift
//  Investigation
//
//  Created by Brian Lane on 10/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit

class AsyncLoadableImageView: UIImageView {
    
    override var intrinsicContentSize: CGSize {

        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width

            let ratio = myViewWidth/myImageWidth
            let scaledHeight = myImageHeight * ratio

            return CGSize(width: myViewWidth, height: scaledHeight)
        }

        return CGSize(width: -1.0, height: -1.0)
    }
    
    var imageURLString: String?
    
    func loadImage(with urlString: String) {
        imageURLString = urlString
        guard let url = URL(string: urlString) else { return }
        let code = String(urlString.split(separator: "\\").last!)
        let path = getDocumentsDirectory().appendingPathComponent("\(code).png")
        var image: UIImage?
        let fm = FileManager.default
        if fm.fileExists(atPath: path.path) {
            image = UIImage(contentsOfFile: path.path)
            if let img = image {
                DispatchQueue.main.async {
                    self.image = img
                }
            }
        } else {
            NetworkController.shared.getImage(from: url) { data in
                image = UIImage(data: data)
                if let img = image {
                    img.saveToDisk(as: code)
                    DispatchQueue.main.async {
                        self.image = img
                    }
                }
            }
        }

    }

}
