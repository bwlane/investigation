//
//  Utils.swift
//  Investigation
//
//  Created by Brian Lane on 10/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation
import UIKit

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}

extension UIImage {
    @discardableResult
    func saveToDisk(as filename: String) -> Bool {
        if let data = self.pngData() {
            let filename = getDocumentsDirectory().appendingPathComponent("\(filename).png")
                try? data.write(to: filename)
                return true
            }
        return false
    }

}
