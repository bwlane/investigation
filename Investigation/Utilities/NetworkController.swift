//
//  NetworkController.swift
//  Investigation
//
//  Created by Brian Lane on 10/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
// A singleton class responsible for handling networking calls and processing network data
//TODO: Rewrite to use injection so we can better test
class NetworkController {
    
    static let shared = NetworkController()
    private init() {} // This is a singleton
    let imageCache = NSCache<NSString, UIImage>() // possibly unneeded depending on final design?
    
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    func getCards(from url: URL, then finished: @escaping ([ArkhamCard]?)-> Void) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            if let cards = self.decodeCards(from: data) {
                finished(cards)
            } else {
                finished(nil)
            }
        }
    }
    
    func getImage(from url: URL, then finished: @escaping (_ data: Data)-> Void) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            finished(data)
        }
    }

    private func decodeCards(from data: Data) -> [ArkhamCard]? {
        var cards: [ArkhamCard]? = nil
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            cards = try decoder.decode([ArkhamCard].self, from: data)
        } catch let error as NSError {
            print("There was an error: \(error.localizedDescription)")
        }
        return cards
    }
}

