//
//  AppDatabase.swift
//  Investigation
//
//  Created by Brian Lane on 10/28/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import GRDB

/// A type responsible for initializing the application database.
///
/// See AppDelegate.setupDatabase()

struct AppDatabase {
    /// Creates a fully initialized database at path
    static func openDatabase(atPath path: String) throws -> DatabaseQueue {
        // Connect to the database
        // See https://github.com/groue/GRDB.swift/blob/master/README.md#database-connections
        let dbQueue = try DatabaseQueue(path: path)
        
        // Define the database schema
        try migrator.migrate(dbQueue)
        return dbQueue
    }
    
    /// The DatabaseMigrator that defines the database schema.
    ///
    /// See https://github.com/groue/GRDB.swift/blob/master/README.md#migrations
    static var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()
        
        migrator.registerMigration("createArkhamCard") { db in
            // Create a table
            // See https://github.com/groue/GRDB.swift#create-tables
            try db.create(table: "arkhamCard") { t in
                t.autoIncrementedPrimaryKey("id")
                
                // See https://github.com/groue/GRDB.swift/blob/master/README.md#unicode
                t.column("name", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("realName", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("typeCode", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("typeName", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("packCode", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("packName", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("factionName", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("factionCode", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("position", .integer).notNull()
                t.column("exceptional", .boolean).notNull()
                t.column("code", .text).notNull().collate(.localizedCaseInsensitiveCompare)
                t.column("cost", .integer)
                t.column("text", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("realText", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("quantity", .integer).notNull()
                t.column("skillIntellect", .integer)
                t.column("skillWillpower", .integer)
                t.column("skillCombat", .integer)
                t.column("skillAgility", .integer)
                t.column("xp", .integer)
                t.column("cluesFixed", .boolean).notNull()
                t.column("healthPerInvestigator", .boolean)
                t.column("deckLimit", .integer)
                t.column("slot", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("traits", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("realTraits", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("flavor", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("illustrator", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("isUnique", .boolean).notNull()
                t.column("doubleSided", .boolean).notNull()
                t.column("backText", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("backFlavor", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("exile", .boolean)
                t.column("hidden", .boolean).notNull()
                t.column("permanent", .boolean).notNull()
                t.column("octgnId", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("url", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("imageSrc", .text).collate(.localizedCaseInsensitiveCompare)
                t.column("backImageSrc", .text).collate(.localizedCaseInsensitiveCompare)
            }
        }
        
        
//        migrator.registerMigration("v1") { db in
//            // Populate the players table with random data
//            for _ in 0..<8 {
//                var card = Card(id: nil, name: Card.randomName(), text: Card.randomText())
//                try card.insert(db)
//            }
//        }
        
        //        // Migrations for future application versions will be inserted here:
        //        migrator.registerMigration(...) { db in
        //            ...
        //        }
        
        return migrator
    }
    
}
