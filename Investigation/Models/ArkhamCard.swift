//
//  ArkhamCard.swift
//  Investigation
//
//  Created by Brian Lane on 10/26/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation
import GRDB

struct ArkhamCard: Codable, FetchableRecord, PersistableRecord {
    
    var id: Int64?
    let name: String
    let realName: String
    let typeCode: String
    let typeName: String
    
    let packCode: String
    let packName: String
    
    let factionCode: String
    let factionName: String
    let position: Int
    let exceptional: Bool
    let code: String
    
    let cost: Int?
    let text: String?
    let realText: String?
    let quantity: Int
    let skillIntellect: Int?
    let skillWillpower: Int?
    let skillCombat: Int?
    let skillAgility: Int?
    let xp: Int?
    let cluesFixed: Bool
    let healthPerInvestigator: Bool?
    let deckLimit: Int?
    let slot: String?
    let traits: String?
    let realTraits: String?
    let flavor: String?
    let illustrator: String?
    let isUnique: Bool
    let doubleSided: Bool
    let backText: String?
    let backFlavor: String?
    let exile: Bool?
    let hidden: Bool
    let permanent: Bool
    let octgnId: String?
    let url: String?
    let imagesrc: String?
    let backimagesrc: String?
}


extension ArkhamCard {
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
