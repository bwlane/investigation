//
//  Card.swift
//  Investigation
//
//  Created by Brian Lane on 10/28/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation
import GRDB

struct Card {
    var id: Int64?
    var name: String
    var code: String
    
    
}

extension Card: Hashable {}

extension Card: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let name = Column(CodingKeys.name)
    }
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
    
}

// MARK: - Card Randomization

extension Card {
    
    private static let names = ["Bob", "Tiffany", "Winslow", "Henrietta", "George", "Pauline", "Bertrude", "Grandella"]
    
    private static let texts = ["This is cool", "A good card", "Take another turn", "Counter spell", "Draw 1 card", "Draw 2 cards", "Return a card from your discard pile to your hand", "Return a card from your discard pile to play under its owner's control."]
    
    static func randomName() -> String {
        return names.randomElement()!
    }
    
    static func randomText() -> String {
        return texts.randomElement()!
    }
}
